# Assignment 4 - Angular - Pokemon Trainer

[![web](https://img.shields.io/static/v1?logo=heroku&message=Online&label=Heroku&color=430098)](https://pokemon-angular-app.herokuapp.com/get-started)

## Assignment
This application is a single page web app using the Angular Framework. This project is part of Noroff Frontend Traneeship. The assignment requires knowlegde of the js framework's fundamentals, specifically Angular, which covers fetching an API, components structure, dealing with modules and lifecicle hooks. 

Note: We have extended the functionality compared to the assignment requirements, we are not storing only the pokemon names but the pokemon object.

## Use the app click on the link bellow
https://pokemon-angular-app.herokuapp.com/get-started

## Running the application locally
- Clone this repository in your prefered IDE
- In the terminal, run the following commands: 
```npm install; npm run serve```

## Design Component tree
![Component tree](src/assets/images/component-tree-pokemon.png)

## Contributing
We are open to receive advice and tips which can help me to improve the design, the logic, to clean up the code, etc. So that we can improve ourselves as a developer.

## Built With
[Intelij](https://www.jetbrains.com/idea/)

## Technologies
- Angular
- TypeScript
- HTML
- CSS

## Author
[Petar Dimitrov](https://gitlab.com/PetarDimitrov91)

[Igor Figueiredo](https://gitlab.com/Igor-GF)

## Acknowledgment
[Dean von Schoultz](https://gitlab.com/deanvons) who is the tutor of the Frontend course at Noroff.

## Project status
Project exclusively made for study purposes.

## License
[MIT](https://choosealicense.com/licenses/mit/)
