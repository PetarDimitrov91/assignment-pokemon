import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {LandingViewComponent} from "./landing-page/landing-view/landing-view.component";
import {CatalogueViewComponent} from "./catalogue-page/catalogue-view/catalogue-view.component";
import {ProfileViewComponent} from "./profile-page/profile-view/profile-view.component";
import {AuthActivate} from "./core/guards/auth.activate";

const routes:Routes =[
  {
  path:'',
    pathMatch:'full',
    redirectTo:'/get-started'
  },
  {
    path:'get-started',
    component: LandingViewComponent
  },
  {
    path:'catalogue',
    component: CatalogueViewComponent,
    canActivate: [AuthActivate],
    data:{
      authenticationRequired: true,
      authenticationFailureRedirectUrl: '/'
    }
  },
  {
    path:'profile',
    component: ProfileViewComponent,
    canActivate: [AuthActivate],
    data:{
      authenticationRequired: true,
      authenticationFailureRedirectUrl: '/'
    }
  }
];

@NgModule({
  imports:[
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule{}
