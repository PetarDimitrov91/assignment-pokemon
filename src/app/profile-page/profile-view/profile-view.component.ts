import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../core/auth.service";
import {IPokemon} from "../../shared/intefaces/pokemon";

@Component({
  selector: 'app-profile-view',
  templateUrl: './profile-view.component.html',
  styleUrls: ['./profile-view.component.css']
})
export class ProfileViewComponent {

  pokemons: Array<IPokemon> | undefined;
  name: string;

  constructor(private authService: AuthService) {
    this.pokemons = this.authService.actualUser?.pokemon!;
    this.name = this.authService.actualUser?.username!;
  }
}
