import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from "@angular/router";
import { ProfileViewComponent } from './profile-view/profile-view.component';
import {SharedModule} from "../shared/shared.module";

@NgModule({
  declarations: [
    ProfileViewComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule
  ]
})
export class ProfilePageModule { }
