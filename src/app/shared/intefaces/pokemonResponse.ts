import {IPokemonPageObj} from "./pokemonInterface";

export interface IPokemonResponse{
  count:number,
  next:string,
  previous: string,
  results:[ Object:IPokemonPageObj]
}
