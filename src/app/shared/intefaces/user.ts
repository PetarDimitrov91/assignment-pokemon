import {IPokemon} from "./pokemon";

export interface IUser {
  id: number,
  username: string,
  pokemon: IPokemon[]
}
