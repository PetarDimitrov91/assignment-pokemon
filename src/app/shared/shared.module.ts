import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PokemonCardComponent} from "./pokemon-card/pokemon-card.component";
import {PokemonService} from "./pokemon.service";

@NgModule({
  declarations: [
    PokemonCardComponent
  ],
  imports: [
    CommonModule,
  ],

  exports: [
    PokemonCardComponent
  ],
  providers:[
    PokemonService
  ]
})
export class SharedModule { }
