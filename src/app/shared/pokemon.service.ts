import {Injectable} from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {IPokemonResponse} from "./intefaces/pokemonResponse";

const POKEMON_API = environment.pokemonApiURL;
const POKEMON_IMG_URL = environment.pokemonImageURL;

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  constructor(private httpClient: HttpClient) {}

  getPageOfPokemons(limit:number, size: number):Observable<IPokemonResponse>{
    return this.httpClient.get<IPokemonResponse>(`${POKEMON_API}/pokemon?limit=${limit}&offset=${size}`);
  }
}
