import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {IPokemon} from "../intefaces/pokemon";
import {AuthService} from "../../core/auth.service";
import {IUser} from "../intefaces/user";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-pokemon-card',
  templateUrl: './pokemon-card.component.html',
  styleUrls: ['./pokemon-card.component.css'],
})
export class PokemonCardComponent {
  @Input() pokemon: IPokemon | undefined;
  context: string | undefined;
  clicked: boolean;
  display: string;

  constructor(
    private authService: AuthService,
    private activatedRoute: ActivatedRoute,
  ) {
    this.display = '';
    this.clicked = false;
    this.context = this.activatedRoute.snapshot.routeConfig?.path;
  }

  hasPokemon(pokemon:IPokemon): boolean{
    let userPokemons: Array<number> = this.authService.actualUser!.pokemon
      .map(e => e.id);

    return userPokemons.includes(pokemon.id);
  }

  collectPokemon( pokemon: IPokemon) {
    if(this.hasPokemon(pokemon)){
      alert('You already have it');
      return;
    }
    this.clicked = true;

    let user: IUser | undefined = this.authService.actualUser;
    user?.pokemon.push(pokemon);

    this.authService.updateUserInLocalStorage(user!);
    this.authService.updateUser(user!.id, user!.pokemon);
  }

  removePokemon(a: IPokemon) {
    let user: IUser | undefined = this.authService.actualUser;
    let index: number = user!.pokemon.indexOf(a);

    user!.pokemon.splice(index, 1);

    this.authService.updateUserInLocalStorage(user!);
    this.authService.updateUser(user!.id, user!.pokemon);
  }
}
