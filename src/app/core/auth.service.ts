import {Inject, Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {LocalStorage} from "./injection-token";
import {IUser} from "../shared/intefaces/user";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";
import {Router} from "@angular/router";
import {IPokemon} from "../shared/intefaces/pokemon";

const USER_API_URL = environment.userApiURL;
const API_KEY = environment.apiKey;

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user: IUser | undefined;

  get isLogged(): boolean {
    return !!this.user;
  }

  constructor(
    @Inject(LocalStorage) private localStorage: Window['localStorage'],
    private httpClient: HttpClient,
    private router: Router
  ) {
    try {
      const localStorageUser = this.localStorage.getItem('<USER>') || 'ERROR';
      this.user = JSON.parse(localStorageUser);
    } catch (e) {
      this.user = undefined;
    }
  }

  login(username: string): void {
    let users = this.httpClient.get<Array<IUser>>(`${USER_API_URL}?username=${username}`);

    users.subscribe(data => {
      if (data.length < 1) {
        this.register(username).subscribe(data => {
          this.user = data;
          this.storeUserIntoLocalStorage(this.user);
          this.router.navigate(['/catalogue']);
        });

      } else {
        this.user = data[0]
        this.storeUserIntoLocalStorage(this.user);
        this.router.navigate(['/catalogue']);
      }
    });
  }

  register(username: string): Observable<IUser> {
    const headers = new HttpHeaders({
      'X-API-Key': API_KEY,
      'Content-Type': 'application/json'
    });

    return this.httpClient.post<IUser>(USER_API_URL, JSON.stringify({
        username: username,
        pokemon: []
      }),
      {
        headers: headers
      });
  }

  updateUser(userId: number, pokemons: IPokemon[]) {
    const headers = new HttpHeaders({
      'X-API-Key': API_KEY,
      'Content-Type': 'application/json'
    });

    this.httpClient.patch<IUser>(`${USER_API_URL}/${userId}`, JSON.stringify({
        pokemon: pokemons
      }),
      {
        headers: headers
      }).subscribe({
      next: (data) => {
      },
      error: (error) => console.error(error)
    });
  }

  private storeUserIntoLocalStorage(user: IUser) {
    this.localStorage.setItem('<USER>', JSON.stringify(user));
  }

  updateUserInLocalStorage(user: IUser) {
    this.localStorage.removeItem('<USER>');
    this.localStorage.setItem('<USER>', JSON.stringify(user));
    this.user = user;
  }

  get actualUser(): IUser | undefined {
    return this.user;
  }

  logout(): void {
    this.user = undefined;
    this.localStorage.removeItem('<USER>');
    this.router.navigate(['/get-started']);
  }
}
