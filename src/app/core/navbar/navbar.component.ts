import {Component, ViewEncapsulation} from '@angular/core';
import {AuthService} from "../auth.service";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
  encapsulation: ViewEncapsulation.Emulated
})
export class NavbarComponent {
  get isUserLoggedIn(): boolean {
    return this.authService.isLogged;
  }

  constructor(private authService: AuthService) {}

  logout() {
    return this.authService.logout();
  }

}
