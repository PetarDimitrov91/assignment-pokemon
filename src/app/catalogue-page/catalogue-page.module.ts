import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from "@angular/router";
import { CatalogueViewComponent } from './catalogue-view/catalogue-view.component';
import {SharedModule} from "../shared/shared.module";



@NgModule({
  declarations: [
    CatalogueViewComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule
  ]
})
export class CataloguePageModule { }
