import {Component} from '@angular/core';
import {PokemonService} from "../../shared/pokemon.service";
import {map} from "rxjs";
import {environment} from "../../../environments/environment";
import {IPokemon} from "../../shared/intefaces/pokemon";
import {AuthService} from "../../core/auth.service";

const POKEMON_IMG_URL = environment.pokemonImageURL;

@Component({
  selector: 'app-trainer-view',
  templateUrl: './catalogue-view.component.html',
  styleUrls: ['./catalogue-view.component.css']
})
export class CatalogueViewComponent {

  pokemons: Array<IPokemon> | undefined;
  limit: number;
  size: number;

  get pokemonsCatalogue(): Array<IPokemon> | undefined{
    return this.pokemons;
  }

  hasPokemon(pokemon:IPokemon): boolean{
    let userPokemons: Array<number> = this.authService.actualUser!.pokemon
      .map(e => e.id);

    return userPokemons.includes(pokemon.id);
  }

  constructor(
    private authService: AuthService,
    private pokemonService: PokemonService) {

    this.limit = 10;
    this.size = 0;
    this.loadPokemons();
  }

  loadNextPage(): void {
    this.size += 10;
    this.loadPokemons();
  }

  loadPreviousPage(): void {
    this.size  -= 10;
    this.loadPokemons();
  }

  loadPokemons(): void {
    this.pokemonService.getPageOfPokemons(this.limit, this.size)
      .pipe(
        map((pokes, index) => {
          return pokes.results.map((e, i) => {
            let id: number = Number(e.url.split('/')[6]);

            return {
              id: id,
              name: e.name,
              url: e.url,
              imageUrl: `${POKEMON_IMG_URL}/${id}.png`
            }
          });
        })
      )
      .subscribe(data => {
        let pokemons: number[] = this.authService.actualUser!.pokemon
          .map(e => e.id);

        data.filter(e => e.id !== 1);
        this.pokemons = data;
      });
  }
}
