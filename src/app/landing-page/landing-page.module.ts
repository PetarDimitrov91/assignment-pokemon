import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from "@angular/router";
import { LandingViewComponent } from './landing-view/landing-view.component';
import {AuthService} from "../core/auth.service";
import {ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    LandingViewComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule
  ],
  providers:
    [
      AuthService
    ]
})
export class LandingPageModule { }
