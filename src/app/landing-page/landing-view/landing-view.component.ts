import {Component, OnInit} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {AuthService} from "../../core/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-landing-view',
  templateUrl: './landing-view.component.html',
  styleUrls: ['./landing-view.component.css']
})
export class LandingViewComponent implements OnInit{

  loginForm: FormGroup

  constructor(private router: Router, private userService: AuthService) {
    this.loginForm = new FormGroup({
      username: new FormControl('', [Validators.required, Validators.minLength(2)])
    });
  }

  getUsers(): void {
    let username: string = this.loginForm.get('username')?.value;
    this.userService.login(username);
  }

  ngOnInit(): void {
    if(this.userService.isLogged){
      this.router.navigate(['/catalogue']).catch(e => console.log(e));
    }
  }
}
